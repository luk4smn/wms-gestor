/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.database.DatabaseManager;
import com.brazil.wms.gestor.model.Product;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas
 */
public class ProductService {
    private static ProductService single_instance = null;
    Product product = new Product();
    
    protected EntityManager entityManager = SessionService.getInstance().getEntityManager();
    DatabaseManager database = new DatabaseManager();

     public static ProductService getInstance(){ 
        if (single_instance == null) 
            single_instance = new ProductService(); 

        return single_instance; 
    }
       
    public Product getProduct(){
        return product;
    }
    
    
    public void seachProduct(String input){
       String query; 
       try {
           switch (input.length()) {
                case 6:
                   query = "select top 1 * from product "
                           + "where cod_aux = '"+input+"' "
                           + "or referencia like '%"+input+"%' "
                           ;
                    break;
                case 10:
                    query = "select top 1 * from product "
                           + "where cod_mat = '"+input+"' "
                           + "or referencia like '%"+input+"%' "
                           ;
                    break;
                case 13:
                    query = "select top 1 * from product left join product_codebar on product.cod_mat = product_codebar.cod_mat "
                            + "where product_codebar.cod_bar = '"+input+"' "
                            ;
                    break;
                case 14:
                    query = "select top 1 * from product left join product_codebar on product.cod_mat = product_codebar.cod_mat "
                        + "where product_codebar.cod_bar = '"+input+"' "
                        ;
                    break;
                default:
                    query = "select top 1 * from product "
                           + "where referencia like '%"+input+"%' "
                           + "or cod_aux = '"+input+"' "
                           ;
                    break;
           }
           
            database.setConnectPath(1);
            database.connect();
            database.runSQL(query);      
            if (database.rs.first()) {
                product.setCod_aux(database.rs.getString("cod_aux"));
                product.setDescricao(database.rs.getString("descricao"));
                product.setNome_fornecedor(database.rs.getString("nome_fornecedor"));
                product.setUnidade_saida(database.rs.getString("unidade_saida"));
                product.setEstoque_matriz(database.rs.getInt("estoque_matriz"));
                product.setEstoque_jp(database.rs.getInt("estoque_jp"));
                product.setUser_field1(database.rs.getString("user_field1"));
                product.setUser_field2(database.rs.getString("user_field2"));
                
            }
            else{
                JFrame jf = new JFrame();
                jf.setAlwaysOnTop(true);
                JOptionPane.showMessageDialog(jf, "PRODUTO NÃO ENCONTRADO");
                product = new Product();
            }
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO REALIZAR CONSULTA DE PRODUTO \nErro !:" + ex);
            LogService.getInstance().generateLog(RecountService.class, ex.getMessage());
        }
        
        database.disconnect(); 
    } 
    
    
    
    public String updateLocal(String input, String column){
        try {
            PreparedStatement pst;
            database.setConnectPath(2);
            database.connect();   
            pst = database.connection.prepareStatement("UPDATE CADMATERIAL SET "+column+" = ? WHERE CODIGOAUXILIAR = ?");
            pst.setString(1, input);
            pst.setString(2, product.getCod_aux());
            pst.execute();
            database.disconnect();
            database.setConnectPath(1);
            
            seachProduct(product.getCod_aux());

            return "Success";

        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO INSERIR \nErro !:" + ex);
        }
              
        return "fail";
    }
    
}
