/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.model.FilialRecountItem;
import com.brazil.wms.gestor.model.FilialRecountProduct;
import java.awt.HeadlessException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.transaction.Transactional;

/**
 *
 * @author lucas.nunes
 */
public class FilialRecountItemService {
    private static FilialRecountItemService single_instance = null;
    FilialRecountItem recountItem = new FilialRecountItem(); 
    FilialRecountProduct recountProduct = new FilialRecountProduct();
    
    protected EntityManager entityManager = SessionService.getInstance().getEntityManager();

     public static FilialRecountItemService getInstance(){ 
        if (single_instance == null) 
            single_instance = new FilialRecountItemService(); 

        return single_instance; 
    }
     
    public void seachRecountProduct(String input){
       String sql;
       recountProduct = new FilialRecountProduct();

       try {
           switch (input.length()) {
               case 6:
                   sql = "select top 1 * from filial_recount_products "
                           + "where cod_aux = '"+input+"' "
                           + "and recount_id ="+FilialRecountService.getInstance().getCurrentRecount().getId()+" "
                           + "or reference like '%"+input+"%' "
                           + "and recount_id ="+FilialRecountService.getInstance().getCurrentRecount().getId()+" ";
                   break;
               case 10:
                   sql = "select top 1 * from filial_recount_products "
                           + "where cod_mat = '"+input+"' "
                           + "and recount_id ="+FilialRecountService.getInstance().getCurrentRecount().getId()+" "
                           + "or reference like '%"+input+"%' "
                           + "and recount_id ="+FilialRecountService.getInstance().getCurrentRecount().getId()+" ";
                   break;
               default:
                   sql = "select top 1 * from filial_recount_products left join product_codebar on filial_recount_products.cod_mat = product_codebar.cod_mat "
                           + "where product_codebar.cod_bar = '"+input+"' "
                           + "and recount_id ="+FilialRecountService.getInstance().getCurrentRecount().getId()+" "
                           + "or reference like '%"+input+"%' "
                           + "and recount_id ="+FilialRecountService.getInstance().getCurrentRecount().getId()+" ";
                   break;
           }
            
            Query query = entityManager.createNativeQuery(sql, FilialRecountProduct.class);
            
            if (!query.getResultList().isEmpty()) {                           
                recountProduct = (FilialRecountProduct) query.getSingleResult();
                entityManager.refresh(recountProduct);
            }
            else{
                JFrame jf = new JFrame();
                jf.setAlwaysOnTop(true);
                JOptionPane.showMessageDialog(jf, "PRODUTO NÃO ENCONTRADO");
            }
        } catch (HeadlessException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO REALIZAR CONSULTA DE PRODUTO \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
        }
    } 
    
    
    public FilialRecountItem getRecountItem(){
        return recountItem;
    }
    
    public FilialRecountProduct getRecountProduct(){
        return recountProduct;
    }
    
    @Transactional
    public String createRecountItem(){
        LocalDateTime now = LocalDateTime.now();
        Timestamp sqlNow = Timestamp.valueOf(now);
        
        try {           
            recountItem.setCreatedAt(sqlNow);

            entityManager.getTransaction().begin();
            entityManager.merge(recountItem);
            entityManager.flush();
            entityManager.getTransaction().commit();
              
            return "Success";
            
        } catch (Exception ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO INSERIR \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
            entityManager.getTransaction().rollback();
        }
        
        return "fail";
    }
}
