/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.model.FilialNote;
import com.brazil.wms.gestor.model.FilialRecount;
import com.brazil.wms.gestor.model.RecountStatus;
import com.brazil.wms.gestor.model.Status;
import java.awt.HeadlessException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.transaction.Transactional;


/**
 *
 * @author lucas.nunes
 */
public class FilialRecountService{
    
    FilialRecount recount = new FilialRecount(); 
    public Map<String, String> recountTotals = new HashMap<>();   
    
    private static FilialRecountService single_instance = null;
    protected EntityManager entityManager = SessionService.getInstance().getEntityManager();
    
    public static FilialRecountService getInstance()
    { 
        if (single_instance == null) 
            single_instance = new FilialRecountService(); 
        
        return single_instance; 
    }
    
    public FilialRecount getCurrentRecount(){
        return recount;
    }
    
    public void clearObject(){
        recount = new FilialRecount();
    }
    
    public String getRecountQuantityArray(String index){
        return recountTotals.get(index);
    }
     
    public void getPendingRecount(){
        try 
        {
            Query query = entityManager.createNamedQuery("findFilialRecounts");
            query.setParameter(1,SessionService.getInstance().user.getId());
            query.setParameter(2,Status.CONFERENCIA_EM_ANDAMENTO.getValor());
            
            if (query.getResultList().size() > 0) {               
                recount = (FilialRecount) query.getSingleResult();
            }
        } 
        catch (Exception ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO CONSULTAR PENDENTES \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
        }
        
    }
    
    @Transactional
    public void create(ArrayList notes){
        try 
        {
            LocalDateTime now = LocalDateTime.now();
            Timestamp sqlNow = Timestamp.valueOf(now);
         
            recount.setUser_id(SessionService.getInstance().user.getId());
            recount.setStatus(Status.CONFERENCIA_EM_ANDAMENTO.getValor());
            recount.setCreatedAt(sqlNow);
            
            entityManager.getTransaction().begin();
            entityManager.persist(recount);
            entityManager.flush();
            entityManager.getTransaction().commit();
            
            entityManager.refresh(recount);
            
            instanceRecount();
            notes.forEach((note_id) -> FilialNoteService.getInstance().attachRecount((int) note_id, (Integer) recount.getId()));
            
        } 
        catch (Exception ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO INSERIR \nErro !:" + ex.getMessage());
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
            entityManager.getTransaction().rollback();
        }   
    }
    
    public void instanceRecount(){
        try 
        {
            Query query = entityManager.createNamedQuery("findFilialRecounts");
            query.setParameter(1,SessionService.getInstance().user.getId());
            query.setParameter(2,Status.CONFERENCIA_EM_ANDAMENTO.getValor());

            if (query.getResultList().size() > 0) {               
                recount = (FilialRecount) query.getSingleResult();
            }
            
        } 
        catch (Exception ex) 
        {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO INSTANCIAR \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
        }
    }
    
    @Transactional
    public void finalizeRecount(String mode) {
        String message = "CONFIRMAR FINALIZAÇÃO\n DE CONFERÊNCIA?";
        
        if (mode.equals("hasPending")) {
            message = "FINALIZAR CONFERÊNCIA\n COM DIFERENÇAS?";
        }
                
        try {
            LocalDateTime now = LocalDateTime.now();
            Timestamp sqlNow = Timestamp.valueOf(now);
            Object[] options = { "Sim", "Não" };
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            int values = JOptionPane.showOptionDialog(jf, message, "Atenção", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
            switch (values) {
                case 0:
                    recount.setStatus(Status.CONFERENCIA_CONCLUIDA.getValor());
                    recount.setUpdatedAt(sqlNow);
        
                    entityManager.getTransaction().begin();
                    entityManager.persist(recount);
                    entityManager.flush();
                    entityManager.getTransaction().commit();
                    entityManager.refresh(recount);
                    
                    //PREPARAÇÃO DE UPDATE DO STATUS DA CONFERENCIA NO ARGOS:
                    prepareToUpdateBlindNoteConferenceStatus();
                    
                    JOptionPane.showMessageDialog(jf, "CONFERÊNCIA FINALIZADA\n COM SUCESSO");
                    recount = new FilialRecount();
                    break;
                case 1:
                    break;
            }    
            
        } catch (HeadlessException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO FINALIZAR \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
            entityManager.getTransaction().rollback();
        } 
    }
    
    public String getNoteNumbers(){
        String notes = "";
        try {
            List<FilialNote> notesList = FilialNoteService.getInstance().getNoteNumbersByRecountId(recount.getId());         
            
            if(!notesList.isEmpty()){
                notes = notesList.stream().map(item -> item.getNfe()+" ,").reduce(notes, String::concat);
            }
              
        } catch (Exception ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO RETORNAR DADOS \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
        }
        
        return notes;
    }
    
    public int countDiffs(){
        try {
            int value = (int) entityManager.createNativeQuery(
                    "select count(recount_id) as value\n" +
                    "from filial_recount_products \n" +
                    "where ((total_quantity_confer - quantity) is null or (total_quantity_confer - quantity) <> 0)\n" +
                    "and (recount_id  = "+recount.getId()+")\n")
                    .getSingleResult();
                      
            return value;
            
        } catch (Exception ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO CONTAR ITENS \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
        }
        
        return 0;
    }
    
    public void prepareToUpdateBlindNoteConferenceStatus(){
        try {
            List<FilialNote> notesList = FilialNoteService.getInstance().getNoteNumbersByRecountId(recount.getId());         
            
            if(!notesList.isEmpty()){
                notesList.forEach(item -> 
                        FilialNoteService.getInstance().updateBlindNoteConferenceStatus(
                                item.getMoviment_order(),
                                RecountStatus.STATUS_CONFERIDO.getValue()
                        )
                );
            }
              
        } catch (Exception ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO SETAR STATUS \nErro !:" + ex);
            LogService.getInstance().generateLog(FilialRecountService.class, ex.getMessage());
        }
    }
    
}
