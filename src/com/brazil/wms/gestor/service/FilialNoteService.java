/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.database.DatabaseManager;
import com.brazil.wms.gestor.model.FilialNote;
import com.brazil.wms.gestor.model.RecountStatus;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.transaction.Transactional;

/**
 *
 * @author lucas.nunes
 */
public class FilialNoteService {
    
    FilialNote note = new FilialNote(); 
    private static FilialNoteService single_instance = null;
    protected EntityManager entityManager = SessionService.getInstance().getEntityManager();
    DatabaseManager database = new DatabaseManager();
    
    public static FilialNoteService getInstance()
    { 
        if (single_instance == null) 
            single_instance = new FilialNoteService(); 
        
        return single_instance; 
    }
    
    public FilialNote getById(final int id) {
        return entityManager.find(FilialNote.class, id);
    }

    @Transactional
    public void attachRecount(int note_id, Integer recount_id){
        try {
            LocalDateTime now = LocalDateTime.now();
            Timestamp sqlNow = Timestamp.valueOf(now);
            
            note = getById(note_id);
            note.setRecount_id(recount_id);
            note.setUpdatedAt(sqlNow);
            
            entityManager.getTransaction().begin();
            entityManager.merge(note);
            entityManager.flush();
            entityManager.getTransaction().commit();
            
            entityManager.refresh(note);
            
            //UPDATE DE STATUS DA CONFERENCIA NO ARGOS:
            updateBlindNoteConferenceStatus(note.getMoviment_order(), RecountStatus.STATUS_EM_CONFERENCIA.getValue());
            
        } catch (Exception  ex) {
            entityManager.getTransaction().rollback();
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO ATUALIZAR IDS DE REF \nErro !:" + ex.getMessage());
            LogService.getInstance().generateLog(FilialNoteService.class, ex.getMessage());
        }
    }
    
    public void updateBlindNoteConferenceStatus(String moviment_order, String status){
        try {
            PreparedStatement pst;
            database.setConnectPath(2);
            
            database.connect();   
            pst = database.connection.prepareStatement("UPDATE MOVIMENTO SET CAMPOLIVREA1 = ? WHERE ORDEMMOVIMENTO = ?");
            pst.setString(1, status);
            pst.setString(2, moviment_order);
            pst.execute();
            database.disconnect();
            
            database.setConnectPath(1);
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO SETAR STATUS \nErro !:" + ex);
        }
    }
    
    public List<FilialNote> getNoteNumbersByRecountId(int recount_id){
        Query query = entityManager.createNamedQuery("findFilialNotes");
        query.setParameter("recount_id",recount_id);
        return query.getResultList();
    }
}
