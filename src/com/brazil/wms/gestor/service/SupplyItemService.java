/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.database.DatabaseManager;
import com.brazil.wms.gestor.model.Product;
import com.brazil.wms.gestor.model.SupplyList;
import com.brazil.wms.gestor.model.SupplyListItem;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas.nunes
 */
public class SupplyItemService {
    
    private static SupplyItemService single_instance = null;
    SupplyListItem supplyListItem = new SupplyListItem(); 
    Product product = new Product(); 
    
    DatabaseManager database = new DatabaseManager();

     public static SupplyItemService getInstance(){ 
        if (single_instance == null) 
            single_instance = new SupplyItemService(); 

        return single_instance; 
    }
     
    public void seachProduct(String input){
       String query; 
       try {
            if(input.length() <= 10){
                query = "select top 1 * from product where cod_aux = '"+input+"'";
            } else{
                query = "select top 1 * from product inner join product_codebar on product.cod_mat = product_codebar.cod_mat where product_codebar.cod_bar = '"+input+"'";
            }
            database.connect();
            database.runSQL(query);      
            if (database.rs.first()) {
                product.setCod_aux(database.rs.getString("cod_aux"));
                product.setDescricao(database.rs.getString("descricao"));
                product.setNome_fornecedor(database.rs.getString("nome_fornecedor"));
                product.setUnidade_saida(database.rs.getString("unidade_saida"));
                product.setPreco_venda(database.rs.getFloat("preco_venda"));
                product.setEstoque_matriz(database.rs.getInt("estoque_matriz"));
                product.setEstoque_jp(database.rs.getInt("estoque_jp"));
                product.setEstoque_inartel(database.rs.getInt("estoque_inartel"));
                product.setEstoque_bayeux(database.rs.getInt("estoque_bayeux"));
                product.setEstoque_loja_avaria(database.rs.getInt("estoque_loja_avaria"));
                product.setEstoque_total(database.rs.getInt("estoque_total"));
            }
            else{
                JFrame jf = new JFrame();
                jf.setAlwaysOnTop(true);
                JOptionPane.showMessageDialog(jf, "PRODUTO NÃO ENCONTRADO");
                product = new Product();
            }
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO REALIZAR CONSULTA DE PRODUTO \nErro !:" + ex);
            LogService.getInstance().generateLog(SupplyList.class, ex.getMessage());
        }
        
        database.disconnect(); 
    } 
    
    
    public Product getProduct(){
        return product;
    }
    
    public SupplyListItem getSupplyListItem(){
        return supplyListItem;
    }
    
    public String createListItem(){
        String type = SupplyService.getInstance().getCurrentSupplyList().getType();
        int count_items = SupplyService.getInstance().countItems();
        
        if(type.equals("REPOSIÇÃO") && count_items >= 30){
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "LIMITE DE ITENS ATINGIDO");
            return "limit_exception";
        }
        else{
            LocalDateTime now = LocalDateTime.now();
            Timestamp sqlNow = Timestamp.valueOf(now);

            try {
                PreparedStatement pst;
                database.connect();   
                pst = database.connection.prepareStatement("insert into supply_list_items (cod_aux,quantity,supply_list_id,created_at)values (?,?,?,?)");
                pst.setString(1, supplyListItem.getCod_aux());
                pst.setInt(2, supplyListItem.getQuantity());
                pst.setInt(3, supplyListItem.getSupply_list_id());
                pst.setTimestamp(4, sqlNow);
                pst.execute();
                database.disconnect();

                return "Success";
                
            } catch (SQLException ex) {
                JFrame jf = new JFrame();
                jf.setAlwaysOnTop(true);
                JOptionPane.showMessageDialog(jf, "ERRO AO INSERIR \nErro !:" + ex);
            }
        }
                
        return "fail";
    }
   
     
}
