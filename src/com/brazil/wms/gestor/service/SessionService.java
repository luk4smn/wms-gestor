/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.model.User;
import javax.swing.JOptionPane;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
/**
 *
 * @author lucas.nunes
 */
public class SessionService {
    
    User user = new User(); 
    private static SessionService single_instance = null;
    protected EntityManager entityManager = getEntityManager();
    
    
    public static SessionService getInstance()
    { 
        if (single_instance == null) 
            single_instance = new SessionService(); 
        
        return single_instance; 
    }
     
     
    public EntityManager getEntityManager() 
    {
       if (entityManager == null) {
           EntityManagerFactory factory = Persistence.createEntityManagerFactory("gestorHibernate");
            entityManager = factory.createEntityManager();
       }

       return entityManager;
    }
   
    public User getUser(String username) {
        String SQL_QUERY ="from "+ User.class.getName()+" a where a.local_login=?0";
        Query query = entityManager.createQuery(SQL_QUERY);
        query.setParameter(0,username);
        
        user = (User) query.getSingleResult();  
        return user; 	
    }
     
    public void login(String username, String password){
        try{
            getUser(username);
            if (!user.getLocal_password().equals(password)) {
                user = new User();
                JOptionPane.showMessageDialog(null, "DIGITE A SENHA CORRETAMENTE !");
            }
        }catch(javax.persistence.NoResultException ex){
           JOptionPane.showMessageDialog(null, "ERRO AO REALIZAR LOGIN \n"+ex.getMessage());
        }
        
    }
    
    public User getCurrentUser(){
        return user;
    }
    
}
