/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.database.DatabaseManager;
import com.brazil.wms.gestor.model.Status;
import com.brazil.wms.gestor.model.SupplyList;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas
 */
public class SupplyService{
    
    private static SupplyService single_instance = null;
    SupplyList supplyList = new SupplyList(); 
    DatabaseManager database = new DatabaseManager();
    
     public static SupplyService getInstance(){
        if (single_instance == null) 
            single_instance = new SupplyService(); 
        
        return single_instance; 
    }
    
    public void getPendingSupplyList(){
        try {
            database.connect();
            database.runSQL("select * from supply_list where user_id ="+ SessionService.getInstance().user.getId() + " and status = "+ Status.DIGITANDO.getValor()+" and deleted_at is null");
            if (database.rs.first()) {
                Object[] options = { "Sim", "Não" };
                JFrame jf = new JFrame();
                jf.setAlwaysOnTop(true);
                int values = JOptionPane.showOptionDialog(jf, "DESEJA CONTINUAR \nÚLTIMA LISTA NÃO FINALIZADA ?", "Atenção", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                switch (values) {
                case 0:
                    supplyList.setId(database.rs.getInt("id"));
                    supplyList.setStatus(database.rs.getInt("status"));
                    supplyList.setUser_id(database.rs.getInt("user_id"));
                    supplyList.setLocal(database.rs.getString("local"));
                    supplyList.setType(database.rs.getString("type"));
                    continueList();
                break;
                case 1:
                    supplyList.setId(database.rs.getInt("id"));
                    supplyList.setStatus(Status.PENDENTE_SEPARACAO.getValor());
                    finalizeList("async");
                break;                
                }
            }
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO REALIZAR CONSULTA \nErro !:" + ex);
            LogService.getInstance().generateLog(SupplyList.class, ex.getMessage());
        }
        
        database.disconnect();
    }
    
    public void continueList(){
        supplyList.setFlag(Status.FLAG_SUPPLY_CONTINUE.getValor());
    }
    
    public void finalizeList(String mode){        
        try {
            LocalDateTime now = LocalDateTime.now();
            Timestamp sqlNow = Timestamp.valueOf(now);
            
            PreparedStatement pst;
            database.connect();                          
            pst = database.connection.prepareStatement("update supply_list set status=?, updated_at=? where id=?");
            pst.setInt(1, Status.PENDENTE_SEPARACAO.getValor());
            pst.setTimestamp(2, sqlNow);
            pst.setInt(3, supplyList.getId());
            pst.execute();
            
            if(!mode.equals("async")){
                JFrame jf = new JFrame();
                jf.setAlwaysOnTop(true);
                JOptionPane.showMessageDialog(jf, "LISTA FINALIZADA COM SUCESSO!");
            }
            else{                
                if(countItems() == 0){
                    pst = database.connection.prepareStatement("delete from supply_list where id=?");
                    pst.setInt(1, supplyList.getId());
                    pst.execute();
                }
            }
            
            supplyList = new SupplyList();
            
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO FINALIZAR \nErro !:" + ex);
            LogService.getInstance().generateLog(SupplyList.class, ex.getMessage());
        }
        
        database.disconnect();
    }

    public void create(String local, String type) {
        try {
            LocalDateTime now = LocalDateTime.now();
            Timestamp sqlNow = Timestamp.valueOf(now);
            
            PreparedStatement pst;
            database.connect();   
            pst = database.connection.prepareStatement("insert into supply_list (user_id,status,local,type,created_at)values (?,?,?,?,?)");
            pst.setInt(1, SessionService.getInstance().user.getId());
            pst.setInt(2, Status.DIGITANDO.getValor());
            pst.setString(3, local);
            pst.setString(4, type);
            pst.setTimestamp(5, sqlNow);
            pst.execute();
            database.disconnect();
            
            instanceSupplyList();
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO INSERIR \nErro !:" + ex);
        }       
    }
    
    public SupplyList getCurrentSupplyList(){
        return supplyList;
    }
    
    public void instanceSupplyList(){
        try {
            database.connect();   
            database.runSQL("select * from supply_list where user_id ="+ SessionService.getInstance().user.getId() + " and status ="+ Status.DIGITANDO.getValor()+" and deleted_at is null");            
            if(database.rs.first()){
                supplyList.setId(database.rs.getInt("id"));
                supplyList.setStatus(database.rs.getInt("status"));
                supplyList.setUser_id(database.rs.getInt("user_id"));
                supplyList.setLocal(database.rs.getString("local"));
                supplyList.setType(database.rs.getString("type"));
            }
            
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO RETORNAR DADOS \nErro !:" + ex);
            Logger.getLogger(SupplyService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        database.disconnect();
    }
    
    public int countItems(){
        try {
            database.connect();
            database.runSQL("select count(id) as value from supply_list_products where supply_list_id  = "+supplyList.getId());
            database.rs.first();
            return database.rs.getInt("value");
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO CONTAR ITENS \nErro !:" + ex);
            Logger.getLogger(SupplyService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 0;
    }
    
    public void clearObject(){
        supplyList = new SupplyList();
    }

}
