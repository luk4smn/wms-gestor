/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.database.DatabaseManager;
import com.brazil.wms.gestor.model.FilialRecount;
import com.brazil.wms.gestor.model.Tables;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author lucas.nunes
 */
public class TableService {
    
    DatabaseManager database = new DatabaseManager();
    private static TableService single_instance = null;
    
    public static TableService getInstance()
    { 
        if (single_instance == null) 
            single_instance = new TableService(); 
        
        return single_instance; 
    }
    
    public void fillProvidersTable(String SQL, JTable table) {
       ArrayList dados = new ArrayList();

       String[] columnsArr = new String[]{"Cód", "Fornecedor"};
       database.connect();
       database.runSQL(SQL);
       try {
           database.rs.first();
           do {
               dados.add(new Object[]{
                   database.rs.getString("cod_provider"),
                   database.rs.getString("name_provider"),
               });
           } while (database.rs.next());
       } catch (SQLException ex) {
           JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
           JOptionPane.showMessageDialog(jf, "SEM DADOS PARA EXIBIR" );
           Logger.getLogger(FilialRecount.class.getName()).log(Level.SEVERE, null, ex);
       }
       Tables model = new Tables(dados, columnsArr);
       table.setModel(model);

       table.getColumnModel().getColumn(0).setPreferredWidth(50);
       table.getColumnModel().getColumn(0).setResizable(!false);      

       table.getTableHeader().setReorderingAllowed(true);
       table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
       database.disconnect();
    }
    
    public void fillNotesTable(String SQL, JTable table) {
        ArrayList dados = new ArrayList();

        String[] columnsArr = new String[]{"ID", "NF-e"};
        database.connect();
        database.runSQL(SQL);
        try {
            database.rs.first();
            do {
                dados.add(new Object[]{
                    database.rs.getInt("id"),
                    database.rs.getString("nfe"),
                });
            } while (database.rs.next());
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO RETORNAR DADOS \nErro !:" + ex);
            Logger.getLogger(SupplyService.class.getName()).log(Level.SEVERE, null, ex);
        }
        Tables model = new Tables(dados, columnsArr);
        table.setModel(model);
        
        table.getColumnModel().getColumn(0).setPreferredWidth(110);
        table.getColumnModel().getColumn(0).setResizable(!false);      

        table.getTableHeader().setReorderingAllowed(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        table.setColumnSelectionAllowed(false);
        table.setRowSelectionAllowed(true);
        
        database.disconnect();
    }
    
    public void fillRecountProductsTable(String SQL, JTable table, Map<String, String> recountTotals) {
        ArrayList dados = new ArrayList();
        
        String[] columnsArr = new String[]{"COD", "Produto", "UND", "QTD", "AV", "VOL"};
        database.connect();
        database.runSQL(SQL);
        try {
            database.rs.first();
            do {
                dados.add(new Object[]{database.rs.getString("cod_mat"),
                    database.rs.getString("description"),
                    database.rs.getString("unity"),
                    database.rs.getString("total_quantity_confer"),
                    database.rs.getString("total_quantity_damage"),
                    database.rs.getString("total_volumes")});
                
                    recountTotals.put(database.rs.getString("cod_mat"),database.rs.getString("quantity"));
                
            } while (database.rs.next());
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO RETORNAR DADOS \nErro !:" + ex);
            Logger.getLogger(SupplyService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Tables model = new Tables(dados, columnsArr);        
        table.setModel(model);
                
        
        table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF); 
        table.getColumn(table.getColumnName(0)).setPreferredWidth(100); 
        table.getColumn(table.getColumnName(1)).setPreferredWidth(230); 
        table.getColumn(table.getColumnName(2)).setPreferredWidth(60); 
        table.getColumn(table.getColumnName(3)).setPreferredWidth(100);
        table.getColumn(table.getColumnName(4)).setPreferredWidth(100);
        table.getColumn(table.getColumnName(5)).setPreferredWidth(100);
                
        table.getTableHeader().setReorderingAllowed(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        database.disconnect();
    }
     
    public void fillSupplyTable(String SQL, JTable table) {
        ArrayList dados = new ArrayList();
        
        String[] columnsArr = new String[]{"#","ID", "Produto", "QTD", "UND", "Fornecedor"};
        database.connect();
        database.runSQL(SQL);
        try {
            database.rs.first();
            do {
                dados.add(new Object[]{
                    database.rs.getRow(),
                    database.rs.getInt("cod_aux"),
                    database.rs.getString("descricao"),
                    database.rs.getString("quantity"),
                    database.rs.getString("unidade_saida"),
                    database.rs.getString("nome_fornecedor")});
            } while (database.rs.next());
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO RETORNAR DADOS \nErro !:" + ex);
            Logger.getLogger(SupplyService.class.getName()).log(Level.SEVERE, null, ex);
        }
        Tables model = new Tables(dados, columnsArr);
        table.setModel(model);
        
        /*table.getColumnModel().getColumn(0).setPreferredWidth(30);
        table.getColumnModel().getColumn(0).setResizable(!false);        
        table.getColumnModel().getColumn(1).setPreferredWidth(110);
        table.getColumnModel().getColumn(1).setResizable(!false);
        table.getColumnModel().getColumn(2).setPreferredWidth(250);
        table.getColumnModel().getColumn(2).setResizable(true);
        table.getColumnModel().getColumn(3).setPreferredWidth(70);
        table.getColumnModel().getColumn(3).setResizable(!false);
        table.getColumnModel().getColumn(4).setPreferredWidth(70);
        table.getColumnModel().getColumn(4).setResizable(!false);
        table.getColumnModel().getColumn(5).setPreferredWidth(130);
        table.getColumnModel().getColumn(5).setResizable(!false);*/
        
        table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF); 
        table.getColumn(table.getColumnName(0)).setPreferredWidth(25); 
        table.getColumn(table.getColumnName(1)).setPreferredWidth(55); 
        table.getColumn(table.getColumnName(2)).setPreferredWidth(150); 
        table.getColumn(table.getColumnName(3)).setPreferredWidth(40);
        table.getColumn(table.getColumnName(4)).setPreferredWidth(40);
        table.getColumn(table.getColumnName(5)).setPreferredWidth(100);

        table.getTableHeader().setReorderingAllowed(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        database.disconnect();
    }
    
    
    public void fillPricesTable(String SQL, JTable table) {
        ArrayList dados = new ArrayList();
        
        String[] columnsArr = new String[]{"Filial","Estoque","UND"};
        database.connect();
        database.runSQL(SQL);
        try {
            database.rs.first();
            dados.add(new Object[]{"BRAZIL ATACADO - CG", database.rs.getInt("estoque_matriz"), database.rs.getString("unidade_saida")});
            dados.add(new Object[]{"INARTEL", database.rs.getInt("estoque_inartel"), database.rs.getString("unidade_saida")});
            dados.add(new Object[]{"BAYEUX", database.rs.getInt("estoque_bayeux"), database.rs.getString("unidade_saida")});
            dados.add(new Object[]{"LOJA AVARIA", database.rs.getInt("estoque_loja_avaria"), database.rs.getString("unidade_saida")});
            dados.add(new Object[]{"BRAZIL ATACADO - JP", database.rs.getInt("estoque_jp"), database.rs.getString("unidade_saida")});
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO RETORNAR DADOS \nErro !:" + ex);
            Logger.getLogger(SupplyService.class.getName()).log(Level.SEVERE, null, ex);
        }
        Tables model = new Tables(dados, columnsArr);
        table.setModel(model);
        
        table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF); 
        table.getColumn(table.getColumnName(0)).setPreferredWidth(150); 
        table.getColumn(table.getColumnName(1)).setPreferredWidth(100); 
        table.getColumn(table.getColumnName(2)).setPreferredWidth(60); 

        table.getTableHeader().setReorderingAllowed(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        database.disconnect();
    }
}
