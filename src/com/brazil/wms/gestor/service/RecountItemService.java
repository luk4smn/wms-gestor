/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.service;

import com.brazil.wms.gestor.database.DatabaseManager;
import com.brazil.wms.gestor.model.RecountItem;
import com.brazil.wms.gestor.model.RecountProduct;
import java.awt.HeadlessException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.transaction.Transactional;

/**
 *
 * @author lucas.nunes
 */
public class RecountItemService {
    private static RecountItemService single_instance = null;
    RecountItem recountItem = new RecountItem(); 
    RecountProduct recountProduct = new RecountProduct();
    
    protected EntityManager entityManager = SessionService.getInstance().getEntityManager();
    DatabaseManager database = new DatabaseManager();

     public static RecountItemService getInstance(){ 
        if (single_instance == null) 
            single_instance = new RecountItemService(); 

        return single_instance; 
    }
     
    public RecountItem getRecountItem(){
        return recountItem;
    }
    
    public RecountProduct getRecountProduct(){
        return recountProduct;
    }
    
    public void seachRecountProduct(String input){
       String query; 
       try {
            switch (input.length()) {
                case 6:
                   query = "select top 1 * from recount_products "
                           + "where cod_aux = '"+input+"' "
                           + "and recount_id ="+RecountService.getInstance().getCurrentRecount().getId() +" "
                           + "or reference like '%"+input+"%' "
                           + "and recount_id ="+RecountService.getInstance().getCurrentRecount().getId()+" ";
                   break;
                case 10:
                   query = "select top 1 * from recount_products "
                           + "where cod_mat = '"+input+"' "
                           + "and recount_id ="+RecountService.getInstance().getCurrentRecount().getId()+" "
                           + "or reference like '%"+input+"%' "
                           + "and recount_id ="+RecountService.getInstance().getCurrentRecount().getId()+" ";
                   break;
                default:
                   query = "select top 1 * from recount_products left join product_codebar on recount_products.cod_mat = product_codebar.cod_mat "
                           + "where product_codebar.cod_bar = '"+input+"' "
                           + "and recount_id ="+RecountService.getInstance().getCurrentRecount().getId()+" "
                           + "or reference like '%"+input+"%' "
                           + "and recount_id ="+RecountService.getInstance().getCurrentRecount().getId()+" ";
                                  
                   break;
           }
                       
            database.connect();
            database.runSQL(query);      
            if (database.rs.first()) {
                recountProduct.setCodMat(database.rs.getString("cod_mat"));
                recountProduct.setUnity(database.rs.getString("unity"));
                recountProduct.setUnityExit(database.rs.getString("unity_exit"));
                recountProduct.setDescription(database.rs.getString("description"));
                recountProduct.setQuantity(database.rs.getInt("quantity"));
                recountProduct.setTotalQuantityConfer(database.rs.getInt("total_quantity_confer"));
                recountProduct.setTotalQuantityDamage(database.rs.getInt("total_quantity_damage"));
                recountProduct.setTotalVolumes(database.rs.getInt("total_volumes"));
                recountProduct.setRecountId(database.rs.getInt("recount_id"));
            }
            else{
                JFrame jf = new JFrame();
                jf.setAlwaysOnTop(true);
                JOptionPane.showMessageDialog(jf, "PRODUTO NÃO ENCONTRADO");
                recountItem = new RecountItem();
            }
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO REALIZAR CONSULTA DE PRODUTO \nErro !:" + ex);
            LogService.getInstance().generateLog(RecountService.class, ex.getMessage());
        }
        
        database.disconnect(); 
    } 
  
       
    @Transactional
    public String createRecountItem(){
        LocalDateTime now = LocalDateTime.now();
        Timestamp sqlNow = Timestamp.valueOf(now);
        
        try {           
            recountItem.setCreatedAt(sqlNow);

            entityManager.getTransaction().begin();
            entityManager.merge(recountItem);
            entityManager.flush();
            entityManager.getTransaction().commit();
            
            return "Success";
            
        } catch (Exception ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf, "ERRO AO INSERIR \nErro !:" + ex);
            LogService.getInstance().generateLog(RecountService.class, ex.getMessage());
            entityManager.getTransaction().rollback();
        }
        
        return "fail";
    }
}
