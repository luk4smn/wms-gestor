/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.util;

import javax.swing.text.AttributeSet;  
import javax.swing.text.BadLocationException;  
import javax.swing.text.PlainDocument;  

/**
 *
 * @author lucas.nunes
 */
public class UpperCaseText extends PlainDocument {
    public void insertString(int offset, String str, AttributeSet attr)  
            throws BadLocationException {  
        if (str == null)  
            return;  
        super.insertString(offset, str.toUpperCase(), attr); // Detalhe no str  
    }  
}
