/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

/**
 *
 * @author lucas.nunes
 */
public class SupplyListItem {
    private int id,
                quantity,
                supply_list_id;
    
    private String cod_aux;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the cod_mat
     */
    public String getCod_aux() {
        return cod_aux;
    }

    /**
     * @param cod_aux the cod_mat to set
     */
    public void setCod_aux(String cod_aux) {
        this.cod_aux = cod_aux;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the supply_list_id
     */
    public int getSupply_list_id() {
        return supply_list_id;
    }

    /**
     * @param supply_list_id the supply_list_id to set
     */
    public void setSupply_list_id(int supply_list_id) {
        this.supply_list_id = supply_list_id;
    }
}
