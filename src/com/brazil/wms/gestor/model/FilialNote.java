/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

import com.sun.istack.Nullable;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

/**
 *
 * @author lucas.nunes
 */
@NamedNativeQueries({
    @NamedNativeQuery(
        name = "findFilialNotes",
        query = "select * from filial_notes where recount_id = :recount_id and deleted_at is null",
        resultClass = FilialNote.class
    )
})
@Entity
@Table(name="filial_notes")
public class FilialNote extends Model implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @Column
    private float total;
    @Column
    private String nfe;
    @Column(name="moviment_order")
    private String moviment_order;
    @Column(name="cod_provider")
    private String cod_provider; 
    @Column(name="name_provider")
    private String name_provider;
    @Column(name="operation")
    private String op;
    @Column(name="status_argos")
    private String status_argos;
    @Column
    @Nullable
    private Integer recount_id;
    @Column
    @Nullable
    private Integer company_id;
    @Column(name="created_at", nullable = true)
    private Timestamp  createdAt;
    @Column(name="updated_at")
    private Timestamp  updatedAt;

   
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the total
     */
    public float getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(float total) {
        this.total = total;
    }

    /**
     * @return the moviment_order
     */
    public String getMoviment_order() {
        return moviment_order;
    }

    /**
     * @param moviment_order the moviment_order to set
     */
    public void setMoviment_order(String moviment_order) {
        this.moviment_order = moviment_order;
    }

    /**
     * @return the nfe
     */
    public String getNfe() {
        return nfe;
    }

    /**
     * @param nfe the nfe to set
     */
    public void setNfe(String nfe) {
        this.nfe = nfe;
    }

    /**
     * @return the cod_provider
     */
    public String getCod_provider() {
        return cod_provider;
    }

    /**
     * @param cod_provider the cod_provider to set
     */
    public void setCod_provider(String cod_provider) {
        this.cod_provider = cod_provider;
    }

    /**
     * @return the name_provider
     */
    public String getName_provider() {
        return name_provider;
    }

    /**
     * @param name_provider the name_provider to set
     */
    public void setName_provider(String name_provider) {
        this.name_provider = name_provider;
    }

    /**
     * @return the status_argos
     */
    public String getStatus_argos() {
        return status_argos;
    }

    /**
     * @param status_argos the status_argos to set
     */
    public void setStatus_argos(String status_argos) {
        this.status_argos = status_argos;
    }

    /**
     * @return the recount_id
     */
    public Integer getRecount_id() {
        return recount_id;
    }

    /**
     * @param recount_id the recount_id to set
     */
    public void setRecount_id(Integer recount_id) {
        this.recount_id = recount_id;
    }

    /**
     * @return the operation
     */
    public String getOp() {
        return op;
    }

    /**
     * @param op the operation to set
     */
    public void setOp(String op) {
        this.op = op;
    }

    /**
     * @return the createdAt
     */
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the updatedAt
     */
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt the updatedAt to set
     */
    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return the company_id
     */
    public Integer getCompany_id() {
        return company_id;
    }

    /**
     * @param company_id the company_id to set
     */
    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }
    
    
}
