/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

/**
 *
 * @author lucas.nunes
 */
public enum RecountStatus {
   
    STATUS_NAO_CONFERIDO("Não Conferido"),
    STATUS_EM_CONFERENCIA("Em conferência"),
    STATUS_CONFERIDO("Conferido");
    
    private final String value;
    
    RecountStatus(String valueOption){
        value = valueOption;
    }
    
    public String getValue(){
        return value;
    }
    
}
