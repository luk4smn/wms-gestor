/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

import com.brazil.wms.gestor.database.DatabaseManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas.nunes
 */
public class Model {
    
    private  String table;
    ResultSet result;
    
    DatabaseManager database = new DatabaseManager();
    
    public ResultSet findOrFail(int id) {    
        try {
            database.connect();
            database.runSQL("SELECT * FROM "+table+" WHERE id = '%" + id + "%'");
            if(database.rs.first()){
                result = database.rs;   
            }else{
               JOptionPane.showMessageDialog(null, "A BUSCA NÃO RETORNOU RESULTADOS !"); 
            }
            database.disconnect();
            return result;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "A BUSCA NÃO RETORNOU RESULTADOS !");
        }
        return null;
    }
}
