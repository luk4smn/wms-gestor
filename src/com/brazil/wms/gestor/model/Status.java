/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

/**
 *
 * @author lucas
 */
public enum Status {
    DIGITANDO(1), PENDENTE_SEPARACAO(2), SEPARANDO(3), FINALIZADO(4),              //listas de abastecimento
    FLAG_SUPPLY_NULL(0), FLAG_SUPPLY_CONTINUE(1),
    CONFERENCIA_PENDENTE(1), CONFERENCIA_EM_ANDAMENTO(2),CONFERENCIA_CONCLUIDA(3)  // conferencias
    ;

    private final int valor;
    
    Status(int valorOpcao){
        valor = valorOpcao;
    }
    
    public int getValor(){
        return valor;
    }
}
