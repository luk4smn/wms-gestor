/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

import com.sun.istack.Nullable;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lucas.nunes
 */
@Entity
@Table(name="supply_list_products")
public class SupplyListProduct extends Model implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    private String id;
    
    @Column(name="cod_aux")
    private String codAux;
    
    @Nullable
    @Column(name="quantity")
    private int quantity;
    
    @Column(name="recount_id")
    private int recount_id;   

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the codAux
     */
    public String getCodAux() {
        return codAux;
    }

    /**
     * @param codAux the codAux to set
     */
    public void setCodAux(String codAux) {
        this.codAux = codAux;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the recount_id
     */
    public int getRecount_id() {
        return recount_id;
    }

    /**
     * @param recount_id the recount_id to set
     */
    public void setRecount_id(int recount_id) {
        this.recount_id = recount_id;
    }
    
}
