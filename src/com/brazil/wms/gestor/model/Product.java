/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

/**
 *
 * @author lucas.nunes
 */
public class Product extends Model {
    
    private String cod_mat,
                   cod_aux, 
                   descricao,
                   unidade_entrada, 
                   unidade_saida,
                   referencia,
                   cod_fornecedor,
                   nome_fornecedor,
                   user_field1,
                   user_field2;
    
    private int fator_saida,
                estoque_total,
                estoque_matriz,
                estoque_jp,
                estoque_loja_avaria,
                estoque_inartel,
                estoque_bayeux;
    
    private float preco_venda;

    /**
     * @return the cod_mat
     */
    public String getCod_mat() {
        return cod_mat;
    }

    /**
     * @param cod_mat the cod_mat to set
     */
    public void setCod_mat(String cod_mat) {
        this.cod_mat = cod_mat;
    }

    /**
     * @return the cod_aux
     */
    public String getCod_aux() {
        return cod_aux;
    }

    /**
     * @param cod_aux the cod_aux to set
     */
    public void setCod_aux(String cod_aux) {
        this.cod_aux = cod_aux;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the unidade_entrada
     */
    public String getUnidade_entrada() {
        return unidade_entrada;
    }

    /**
     * @param unidade_entrada the unidade_entrada to set
     */
    public void setUnidade_entrada(String unidade_entrada) {
        this.unidade_entrada = unidade_entrada;
    }

    /**
     * @return the unidade_saida
     */
    public String getUnidade_saida() {
        return unidade_saida;
    }

    /**
     * @param unidade_saida the unidade_saida to set
     */
    public void setUnidade_saida(String unidade_saida) {
        this.unidade_saida = unidade_saida;
    }

    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * @return the cod_fornecedor
     */
    public String getCod_fornecedor() {
        return cod_fornecedor;
    }

    /**
     * @param cod_fornecedor the cod_fornecedor to set
     */
    public void setCod_fornecedor(String cod_fornecedor) {
        this.cod_fornecedor = cod_fornecedor;
    }

    /**
     * @return the nome_fornecedor
     */
    public String getNome_fornecedor() {
        return nome_fornecedor;
    }

    /**
     * @param nome_fornecedor the nome_fornecedor to set
     */
    public void setNome_fornecedor(String nome_fornecedor) {
        this.nome_fornecedor = nome_fornecedor;
    }

    /**
     * @return the user_field1
     */
    public String getUser_field1() {
        return user_field1;
    }

    /**
     * @param user_field1 the user_field1 to set
     */
    public void setUser_field1(String user_field1) {
        this.user_field1 = user_field1;
    }
    
        /**
     * @return the user_field1
     */
    public String getUser_field2() {
        return user_field2;
    }

    /**
     * @param user_field2
     */
    public void setUser_field2(String user_field2) {
        this.user_field2 = user_field2;
    }

    /**
     * @return the fator_saida
     */
    public int getFator_saida() {
        return fator_saida;
    }

    /**
     * @param fator_saida the fator_saida to set
     */
    public void setFator_saida(int fator_saida) {
        this.fator_saida = fator_saida;
    }

    /**
     * @return the estoque_total
     */
    public int getEstoque_total() {
        return estoque_total;
    }

    /**
     * @param estoque_total the estoque_total to set
     */
    public void setEstoque_total(int estoque_total) {
        this.estoque_total = estoque_total;
    }

    /**
     * @return the estoque_matriz
     */
    public int getEstoque_matriz() {
        return estoque_matriz;
    }

    /**
     * @param estoque_matriz the estoque_matriz to set
     */
    public void setEstoque_matriz(int estoque_matriz) {
        this.estoque_matriz = estoque_matriz;
    }

    /**
     * @return the estoque_inartel
     */
    public int getEstoque_inartel() {
        return estoque_inartel;
    }

    /**
     * @param estoque_inartel the estoque_inartel to set
     */
    public void setEstoque_inartel(int estoque_inartel) {
        this.estoque_inartel = estoque_inartel;
    }

    /**
     * @return the estoque_bayeux
     */
    public int getEstoque_bayeux() {
        return estoque_bayeux;
    }

    /**
     * @param estoque_bayeux the estoque_bayeux to set
     */
    public void setEstoque_bayeux(int estoque_bayeux) {
        this.estoque_bayeux = estoque_bayeux;
    }

    /**
     * @return the preco_venda
     */
    public float getPreco_venda() {
        return preco_venda;
    }

    /**
     * @param preco_venda the preco_venda to set
     */
    public void setPreco_venda(float preco_venda) {
        this.preco_venda = preco_venda;
    }

    /**
     * @return the estoque_jp
     */
    public int getEstoque_jp() {
        return estoque_jp;
    }

    /**
     * @param estoque_jp the estoque_jp to set
     */
    public void setEstoque_jp(int estoque_jp) {
        this.estoque_jp = estoque_jp;
    }
    
     /**
     * @return the estoque_loja_avaria
     */
    public int getEstoque_loja_avaria() {
        return estoque_loja_avaria;
    }

    /**
     * @param estoque_loja_avaria the estoque_loja_avaria to set
     */
    public void setEstoque_loja_avaria(int estoque_loja_avaria) {
        this.estoque_loja_avaria = estoque_loja_avaria;
    }
    
    
    
    
    
}
