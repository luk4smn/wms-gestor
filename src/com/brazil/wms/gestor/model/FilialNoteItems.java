/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author lucas.nunes
 */
public class FilialNoteItems extends Model {
    private final String table;
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    private int     quantity,
                    quantity_nfe,
                    factor,
                    note_id;
    
    private String  moviment_order, 
                    cod_mat,
                    unity;

    public FilialNoteItems(){
        table = "filial_note_items";
    }

    /**
     * @return the table
     */
    public String getTable() {
        return table;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the quantity_nfe
     */
    public int getQuantity_nfe() {
        return quantity_nfe;
    }

    /**
     * @param quantity_nfe the quantity_nfe to set
     */
    public void setQuantity_nfe(int quantity_nfe) {
        this.quantity_nfe = quantity_nfe;
    }

    /**
     * @return the factor
     */
    public int getFactor() {
        return factor;
    }

    /**
     * @param factor the factor to set
     */
    public void setFactor(int factor) {
        this.factor = factor;
    }

    /**
     * @return the note_id
     */
    public int getNote_id() {
        return note_id;
    }

    /**
     * @param note_id the note_id to set
     */
    public void setNote_id(int note_id) {
        this.note_id = note_id;
    }

    /**
     * @return the moviment_order
     */
    public String getMoviment_order() {
        return moviment_order;
    }

    /**
     * @param moviment_order the moviment_order to set
     */
    public void setMoviment_order(String moviment_order) {
        this.moviment_order = moviment_order;
    }

    /**
     * @return the cod_mat
     */
    public String getCod_mat() {
        return cod_mat;
    }

    /**
     * @param cod_mat the cod_mat to set
     */
    public void setCod_mat(String cod_mat) {
        this.cod_mat = cod_mat;
    }

    /**
     * @return the unity
     */
    public String getUnity() {
        return unity;
    }

    /**
     * @param unity the unity to set
     */
    public void setUnity(String unity) {
        this.unity = unity;
    }
}
