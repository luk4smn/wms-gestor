/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lucas.nunes
 */
@Entity
@Table(name="filial_recount_items")
public class FilialRecountItem extends Model implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="quantity_confer")
    private int quantityConfer;
            
    @Column(name="quantity_damage")
    private int quantityDamage;
    
    @Column
    private int volumes;
    
    @Column(name="cod_mat")
    private String codMat;
    
    @Column(name="created_at")
    private Timestamp  createdAt;

    @Column(name="recount_id")
    private int recount_id;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the quantityConfer
     */
    public int getQuantityConfer() {
        return quantityConfer;
    }

    /**
     * @param quantityConfer the quantityConfer to set
     */
    public void setQuantityConfer(int quantityConfer) {
        this.quantityConfer = quantityConfer;
    }

    /**
     * @return the quantityDamage
     */
    public int getQuantityDamage() {
        return quantityDamage;
    }

    /**
     * @param quantityDamage the quantityDamage to set
     */
    public void setQuantityDamage(int quantityDamage) {
        this.quantityDamage = quantityDamage;
    }

    /**
     * @return the volumes
     */
    public int getVolumes() {
        return volumes;
    }

    /**
     * @param volumes the volumes to set
     */
    public void setVolumes(int volumes) {
        this.volumes = volumes;
    }

    /**
     * @return the codMat
     */
    public String getCodMat() {
        return codMat;
    }

    /**
     * @param codMat the codMat to set
     */
    public void setCodMat(String codMat) {
        this.codMat = codMat;
    }

    /**
     * @return the createdAt
     */
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the recount
     */
    public int getRecountId() {
        return recount_id;
    }

    /**
     * @param recount the recount to set
     */
    public void setRecountId(int recount) {
        this.recount_id = recount;
    }
    
}
