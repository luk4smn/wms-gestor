/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

import com.sun.istack.Nullable;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lucas.nunes
 */
@Entity
@Table(name="users")

public class User extends Model implements Serializable{    
    
    @Id
    private int id;
    
    @Column
    private String local_login;
    @Column
    private String local_password;
    @Column
    private String name;
    @Column
    @Nullable
    private Integer company_id;
  
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the local_password
     */
    public String getLocal_password() {
        return local_password;
    }

    /**
     * @param local_password the local_password to set
     */
    public void setLocal_password(String local_password) {
        this.local_password = local_password;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the local_login
     */
    public String getLocal_login() {
        return local_login;
    }

    /**
     * @param local_login the local_login to set
     */
    public void setLocal_login(String local_login) {
        this.local_login = local_login;
    }

    /**
     * @return the company_id
     */
    public Integer getCompany_id() {
        return company_id;
    }

    /**
     * @param company_id the company_id to set
     */
    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }
    
    
    
}
