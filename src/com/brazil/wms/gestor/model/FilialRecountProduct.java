/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

import com.sun.istack.Nullable;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author lucas.nunes
 */
@Entity
@Table(name="filial_recount_products")
public class FilialRecountProduct extends Model implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE)
    private String id;
    
    @Column(name="cod_mat")
    private String codMat;
    @Column
    private String description;    
    @Column
    private String unity;
    @Column(name="unity_exit")
    private String unityExit;
    
    @Nullable
    @Column
    private int quantity;
    
    @Nullable
    @Column(name="total_quantity_confer", nullable = true)
    private Integer  totalQuantityConfer;
    
    @Nullable
    @Column(name="total_quantity_damage", nullable = true)
    private Integer  totalQuantityDamage;
    
    @Nullable
    @Column(name="total_volumes", nullable = true)
    private Integer  totalVolumes;
    
    @Column(name="recount_id")
    private int recount_id;
    /**
     * @return the cod_mat
     */
    

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the codMat
     */
    public String getCodMat() {
        return codMat;
    }

    /**
     * @param codMat the codMat to set
     */
    public void setCodMat(String codMat) {
        this.codMat = codMat;
    }

    /**
     * @return the recountId
     */
    public int getRecountId() {
        return recount_id;
    }

    /**
     * @param recountId the recountId to set
     */
    public void setRecountId(int recountId) {
        this.recount_id = recountId;
    }

    /**
     * @return the totalQuantityConfer
     */
    public Integer getTotalQuantityConfer() {
        return totalQuantityConfer;
    }

    /**
     * @param totalQuantityConfer the totalQuantityConfer to set
     */
    public void setTotalQuantityConfer(Integer totalQuantityConfer) {
        this.totalQuantityConfer = totalQuantityConfer;
    }

    /**
     * @return the totalQuantityDamage
     */
    public Integer getTotalQuantityDamage() {
        return totalQuantityDamage;
    }

    /**
     * @param totalQuantityDamage the totalQuantityDamage to set
     */
    public void setTotalQuantityDamage(Integer totalQuantityDamage) {
        this.totalQuantityDamage = totalQuantityDamage;
    }

    /**
     * @return the totalVolumes
     */
    public Integer getTotalVolumes() {
        return totalVolumes;
    }

    /**
     * @param totalVolumes the totalVolumes to set
     */
    public void setTotalVolumes(Integer totalVolumes) {
        this.totalVolumes = totalVolumes;
    }

    /**
     * @return the recount_id
     */
    
    /**
     * @return the unity_exit
     */
    public String getUnityExit() {
        return unityExit;
    }

    /**
     * @param unity_exit the unity_exit to set
     */
    public void setUnityExit(String unity_exit) {
        this.unityExit = unity_exit;
    }

    /**
     * @return the unity
     */
    public String getUnity() {
        return unity;
    }

    /**
     * @param unity the unity to set
     */
    public void setUnity(String unity) {
        this.unity = unity;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }
}
