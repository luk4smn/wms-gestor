/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.model;

/**
 *
 * @author lucas.nunes
 */
public enum Company {
   
    WL_MATRIZ(1),
    BRAZIL_JP(2);
    
    private final int value;
    
    Company(int valueOption){
        value = valueOption;
    }
    
    public int getValue(){
        return value;
    }
    
}
