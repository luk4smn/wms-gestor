/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brazil.wms.gestor.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author Lucas
 */
public class DatabaseManager {

    public Statement stm; //prepara e realiza pesquisa no BD
    public ResultSet rs; //armazena o resultado da pesquisa
    
    private String driver, path, dbUser,dbPassword;
    
    private int connectPath = 1; 
       
    private static final Properties config = new Properties();
    private static final String file = "config.ini";


  /**
     *
     */
    public Connection connection; //conecta o BD
    
    /**
     * Abre conexao cm o BD
     */
    public void connect(){
        try {
            try {
            //tentativa inicial de conexão
            config.load(new FileInputStream(file));
            driver = config.getProperty("driver");
            dbUser = config.getProperty("db_user");
            dbPassword = config.getProperty("db_pass");
            
            if( getConnectPath() == 1){
                path = config.getProperty("path");
            }else{
                path = config.getProperty("path_2");
            }
            
            } catch (IOException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.setProperty("jdbc.Drivers", driver);                             //seta a propriedade do driver de conexão
            Class.forName(driver); 
            connection = DriverManager.getConnection(path, dbUser, dbPassword);    //realiza a conexão cm o BD
            
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf,"ERRO AO ABRIR CONEXÃO \n" +ex);
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            disconnect();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Executa Query
     * @param sql
     */
    public void runSQL (String sql){
        try{
            stm = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stm.executeQuery(sql);
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog (jf, "Erro de Execução SQL!\n Erro" +ex.getMessage());
        }
    }
    
    /**
     * Fecha a conexao do BD
     */
    public void disconnect(){
        try {
          connection.close();
        } catch (SQLException ex) {
            JFrame jf = new JFrame();
            jf.setAlwaysOnTop(true);
            JOptionPane.showMessageDialog(jf,"ERRO AO FECHAR CONEXÃO");
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }

    /**
     * @return the connectPath
     */
    public int getConnectPath() {
        return connectPath;
    }

    /**
     * @param connectPath the connectPath to set
     */
    public void setConnectPath(int connectPath) {
        this.connectPath = connectPath;
    }
}
